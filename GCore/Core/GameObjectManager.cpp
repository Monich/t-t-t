#include "GameObjectManager.hpp"
#include "ShaderManager.hpp"
#include "GraphicsOrchestrator.hpp"
#include "TextureManager.hpp"
#include "Component.hpp"

GameObjectManager::GameObjectManager(GraphicsOrchestrator &graphicsOrchestrator, ShaderManager &shaderManager,
                                     TextureManager &textureManager) :
    m_GraphicsOrchestrator(graphicsOrchestrator),
    m_ShaderManager(shaderManager),
    m_TextureManager(textureManager)
{}

void GameObjectManager::RegisterRenderer(ShaderType type, Renderer *renderer)
{
    m_ShaderManager.RegisterRenderer(type, renderer);
}

void GameObjectManager::DeregisterRenderer(ShaderType type, Renderer *renderer)
{
    m_ShaderManager.DeregisterRenderer(type, renderer);
}

const GpuMesh *GameObjectManager::GetRectMesh()
{
    return m_ShaderManager.GetRectMesh();
}

void GameObjectManager::RegisterMouseCallback(Button *button)
{
    m_GraphicsOrchestrator.RegisterMouseCallback(button);
}

void GameObjectManager::UnregisterMouseCallback(Button *button)
{
    m_GraphicsOrchestrator.UnregisterMouseCallback(button);
}

const Texture *GameObjectManager::GetTexture(const char *path)
{
    return m_TextureManager.LoadTexture(path);
}

void GameObjectManager::OnComponentAdded(Component *component)
{
    m_InitializationQueue.emplace(component);
}

void GameObjectManager::InitializeNewComponents()
{
    while(!m_InitializationQueue.empty())
    {
        auto component = m_InitializationQueue.front();
        component->Initialize(this);
        m_InitializationQueue.pop();
    }
}
