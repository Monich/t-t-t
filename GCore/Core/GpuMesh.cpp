#include "GpuMesh.hpp"
#include <GL/glew.h>

GpuMesh::GpuMesh() : m_IndexCount{-1}, m_IndexBufferId{0}, m_VboBufferId{0}, m_UvBufferId{0}
{
}

GpuMesh::~GpuMesh()
{
    if(m_VboBufferId != 0)
        glDeleteBuffers(1, &m_VboBufferId);
    if(m_IndexBufferId != 0)
        glDeleteBuffers(1, &m_IndexBufferId);
    if(m_UvBufferId != 0)
        glDeleteBuffers(1, &m_UvBufferId);

    m_VboBufferId = 0;
    m_IndexBufferId = 0;
    m_IndexCount = 0;
    m_UvBufferId = 0;
}

void GpuMesh::Upload(const unsigned int *indices, unsigned int indicesCount, const glm::vec2 *vertices,
                     unsigned int verticesCount, const glm::vec2 *uvs, unsigned uvCount)
{
    m_IndexCount = (long)indicesCount;

    glGenBuffers(1, &m_VboBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, m_VboBufferId);
    glBufferData(GL_ARRAY_BUFFER,  (long)(2 * sizeof(float) * verticesCount), vertices, GL_STATIC_DRAW);

    glGenBuffers(1, &m_IndexBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, m_IndexBufferId);
    glBufferData(GL_ARRAY_BUFFER,  (long)(sizeof(unsigned) * indicesCount), indices, GL_STATIC_DRAW);

    glGenBuffers(1, &m_UvBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, m_UvBufferId);
    glBufferData(GL_ARRAY_BUFFER,  (long)(2 * sizeof(float) * uvCount), uvs, GL_STATIC_DRAW);
}

unsigned GpuMesh::GetVboBufferId() const
{
    return m_VboBufferId;
}

unsigned GpuMesh::GetIndexBufferId() const
{
    return m_IndexBufferId;
}

unsigned GpuMesh::GetUvBufferId() const
{
    return m_UvBufferId;
}

long GpuMesh::GetIndexCount() const
{
    return m_IndexCount;
}
