#pragma once

#include "Component.hpp"
#include "RenderData.hpp"
#include "Shaders/ShaderType.hpp"

class Renderer : public Component
{
public:
    ~Renderer() override;

    void Initialize() final;
    void Update() override;

    const RenderData& GetRendererData();

protected:
    explicit Renderer(ShaderType shaderType);
    virtual void InitializeRenderer() = 0;

    RenderData m_RenderData;

private:
    ShaderType m_ShaderType;
    bool m_IsInitialized;

};
