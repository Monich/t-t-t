#include <fstream>
#include "TextureManager.hpp"
#include "Texture.hpp"

TextureManager::TextureManager() = default;

TextureManager::~TextureManager()
{
    for (auto tex : m_TextureDictionary)
    {
        delete tex.second;
    }
}

const Texture *TextureManager::LoadTexture(const char *path)
{
    auto it = m_TextureDictionary.find(path);
    if(it != m_TextureDictionary.end())
        return it->second;

    auto *texture = new Texture();
    if(!texture->LoadFromFile(path))
    {
        delete texture;
        return nullptr;
    }

    m_TextureDictionary[path] = texture;
    return texture;
}
