#pragma once

#include <glm/glm.hpp>

class GpuMesh;
class Texture;

struct RenderData
{
    const glm::mat4 *mvpMatrix;
    const GpuMesh *mesh;
    const Texture *texture;

    glm::vec3 colorData;
};
