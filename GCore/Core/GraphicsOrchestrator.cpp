#include "GraphicsOrchestrator.hpp"
#include "RootObject.hpp"
#include "ShaderManager.hpp"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <il.h>
#include <ilu.h>
#include "Button.hpp"
#include "RectCollider.hpp"
#include "GameObjectManager.hpp"

GraphicsOrchestrator::GraphicsOrchestrator(ShaderManager &m_ShaderManager, RootObject &rootObject,
                                           GameObjectManager &gameObjectManager) :
    m_Window(nullptr),
    m_ShaderManager(m_ShaderManager),
    m_RootObject(rootObject),
    m_GameObjectManager(gameObjectManager)
{
}

GraphicsOrchestrator::~GraphicsOrchestrator()
{
    if(m_Window)
        glfwDestroyWindow(m_Window);
    m_Window = nullptr;
    if(m_VertexArrayObjectID != 0)
        glDeleteVertexArrays(1, &m_VertexArrayObjectID);
    m_VertexArrayObjectID = -1;
    glfwTerminate();
}

bool GraphicsOrchestrator::Prepare()
{
    if (!glfwInit())
        return false;

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    glewExperimental = true;

    m_Window = glfwCreateWindow(640, 480, "GCoreWindow", nullptr, nullptr);
    if(m_Window == nullptr)
        return false;

    glfwSetInputMode(m_Window, GLFW_STICKY_KEYS, GL_TRUE);
    glfwSetWindowUserPointer(m_Window, this);
    glfwMakeContextCurrent(m_Window);

    glfwSetMouseButtonCallback(m_Window, _HandleMouseInput);

    if(glewInit() != GLEW_OK || !m_ShaderManager.Prepare())
        return false;
    glClearColor( 0.f, 0.f, 0.f, 1.f );
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    glGenVertexArrays(1, &m_VertexArrayObjectID);
    glBindVertexArray(m_VertexArrayObjectID);

    GLenum error = glGetError();
    if( error != GL_NO_ERROR )
    {
        std::cout << "Error initializing OpenGL! " << gluErrorString(error) << std::endl;
        return false;
    }

    ilInit();
    ilClearColour(255,255,255,255);
    ILenum ilError = ilGetError();
    if(ilError != IL_NO_ERROR)
    {
        std::cout << "Error initializing DevIL, " << iluErrorString(ilError) << std::endl;
        return false;
    }

    return true;
}

int GraphicsOrchestrator::EnterMailLoop()
{
    while (!ShouldClose())
    {
        m_RootObject.Tick();
        m_GameObjectManager.InitializeNewComponents();

        glClear(GL_COLOR_BUFFER_BIT);
        m_ShaderManager.RunAllRenderers();
        glfwSwapBuffers(m_Window);
        glfwPollEvents();
    }

    return 0;
}

bool GraphicsOrchestrator::ShouldClose()
{
    return glfwWindowShouldClose(m_Window);
}

void GraphicsOrchestrator::RegisterMouseCallback(Button *button)
{
    m_MouseCallbacks.push_front(button);
}

void GraphicsOrchestrator::UnregisterMouseCallback(Button *button)
{
    m_MouseCallbacks.remove(button);
}

void GraphicsOrchestrator::HandleMouseInput(GLFWwindow* window, int button, int action, int mods)
{
    if(action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_1)
    {
        double x, y;
        glfwGetCursorPos(window, &x, &y);
        auto buttonComponent = FindButtonAtPixel(x, y);
        if (buttonComponent)
            buttonComponent->OnPress();
    }
}

void GraphicsOrchestrator::_HandleMouseInput(GLFWwindow *window, int button, int action, int mods)
{
    auto* orchestrator = static_cast<GraphicsOrchestrator*>(glfwGetWindowUserPointer(window));
    orchestrator->HandleMouseInput(window, button, action, mods);
}

Button *GraphicsOrchestrator::FindButtonAtPixel(double x, double y)
{
    for (auto* button : m_MouseCallbacks)
    {
        if(button->GetCollider().Collides(x, y))
            return button;
    }

    return nullptr;
}
