#include "Renderer.hpp"
#include "GameObjectManager.hpp"
#include "GameObject.hpp"

Renderer::Renderer(ShaderType shaderType) : m_ShaderType(shaderType), m_RenderData(), m_IsInitialized(false)
{

}

Renderer::~Renderer()
{
    m_GameObjectManager->DeregisterRenderer(m_ShaderType, this);
}

void Renderer::Initialize()
{
    InitializeRenderer();
    m_IsInitialized = true;
}

void Renderer::Update()
{
    if(m_IsInitialized)
    {
        m_GameObjectManager->RegisterRenderer(m_ShaderType, this);
        m_RenderData.mvpMatrix = m_Parent->GetMvpMatrix();
    }
}

const RenderData &Renderer::GetRendererData()
{
    return m_RenderData;
}
