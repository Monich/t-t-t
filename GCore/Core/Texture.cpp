#include "Texture.hpp"
#include <il.h>
#include <GL/glew.h>

Texture::Texture() : m_TextureId(0)
{
}

Texture::~Texture()
{
    glDeleteTextures(1, &m_TextureId);
}

bool Texture::LoadFromFile(const char *path)
{
    if(m_TextureId != 0)
        return false;

    FILE *file = fopen(path, "r");
    if(file == nullptr)
        return false;
    fclose(file);

    unsigned imageId = ilGenImage();
    ilBindImage(imageId);
    if(!ilLoadImage(path))
    {
        ilDeleteImage(imageId);
        return false;
    }

    if(!ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE))
    {
        ilDeleteImage(imageId);
        return false;
    }

    glGenTextures(1, &m_TextureId);
    glBindTexture(GL_TEXTURE_2D, m_TextureId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 0, GL_RGBA, GL_UNSIGNED_BYTE, ilGetData());
    ilDeleteImage(imageId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);

    return true;
}

unsigned Texture::GetTextureId() const
{
    return m_TextureId;
}
