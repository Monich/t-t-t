#pragma once

#include "ShaderType.hpp"
#include "gcoredefines.hpp"
#include <glm/glm.hpp>

struct RenderData;

class Shader
{
public:
    ~Shader();
    bool Initialize();
    void Use(const glm::mat4& camera);

    virtual void Render(const RenderData&) = 0;

protected:
    virtual const char* GetVertexShaderCode() = 0;
    virtual const char* GetFragmentShaderCode() = 0;

    virtual bool OnInitialized() = 0;

    uint m_ProgramId{0};
    int m_CameraUniform{-1};
};
