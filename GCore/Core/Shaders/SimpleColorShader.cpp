#include <GL/glew.h>
#include "SimpleColorShader.hpp"
#include "Core/RenderData.hpp"
#include "Core/GpuMesh.hpp"

const char *SimpleColorShader::GetVertexShaderCode()
{
    return "#version 330 core\n"
           "in vec2 aVBOHandle;"
           "uniform mat4 uCameraMatrix;"
           "uniform mat4 uMVPMatrix;"
           "void main(){"
           "    gl_Position = uCameraMatrix * uMVPMatrix * vec4(aVBOHandle,0,1);"
           "}";
}

const char *SimpleColorShader::GetFragmentShaderCode()
{
    return "#version 330 core\n"
           "uniform vec3 uColor;"
           "layout(location = 0) out vec3 color;"
           "void main(){"
           "    color = uColor;"
           "}";
}

bool SimpleColorShader::OnInitialized()
{
    m_VboHandle = glGetAttribLocation(m_ProgramId, "aVBOHandle");
    m_MvpMatrix = glGetUniformLocation(m_ProgramId, "uMVPMatrix");
    m_Color = glGetUniformLocation(m_ProgramId, "uColor");
    m_CameraUniform = glGetUniformLocation(m_ProgramId, "uCameraMatrix");

    return m_VboHandle != -1 && m_MvpMatrix != -1 && m_Color != -1;
}

void SimpleColorShader::Render(const RenderData &data)
{
    const auto mvp = *data.mvpMatrix;
    glUniform3fv(m_Color, 1, &data.colorData.x);
    glUniformMatrix4fv(m_MvpMatrix, 1, GL_FALSE, &mvp[0][0]);

    glEnableVertexAttribArray(m_VboHandle);
    glBindBuffer(GL_ARRAY_BUFFER, data.mesh->GetVboBufferId());
    glVertexAttribPointer(m_VboHandle, 2,GL_FLOAT,GL_FALSE,0,nullptr);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, data.mesh->GetIndexBufferId());
    glDrawElements(  GL_TRIANGLES, (int)data.mesh->GetIndexCount(), GL_UNSIGNED_INT, nullptr);

    glDisableVertexAttribArray(m_VboHandle);
}
