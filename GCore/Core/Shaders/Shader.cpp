#include "Shader.hpp"

#include <GL/glew.h>
#include <iostream>

namespace {

uint CompileShader(unsigned int type, const char *shaderCode)
{
    unsigned int shaderId = glCreateShader(type);

    glShaderSource(shaderId, 1, &shaderCode, nullptr);
    glCompileShader(shaderId);

    int result;
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE)
    {
        int logLength;
        glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &logLength);
        char log[logLength];
        glGetShaderInfoLog(shaderId, logLength, nullptr, log);
        std::cout << "Failed to compile shader: " << log << std::endl;

        glDeleteShader(shaderId);
        return 0;
    }
    return shaderId;
}

uint CompileProgram(uint vertexShaderId, uint fragmentShaderId)
{
    uint programId = glCreateProgram();
    glAttachShader(programId, vertexShaderId);
    glAttachShader(programId, fragmentShaderId);
    glLinkProgram(programId);

    int result;
    glGetProgramiv(programId, GL_LINK_STATUS, &result);
    if(result == GL_FALSE)
    {
        int logLength;
        glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &logLength);

        char log[logLength];
        glGetProgramInfoLog(programId, logLength, nullptr, log);
        std::cout << "Failed to link program: " << log << std::endl;
    }

    glDetachShader(programId, vertexShaderId);
    glDetachShader(programId, fragmentShaderId);

    return programId;
}

}

Shader::~Shader()
{
    if(m_ProgramId != 0)
        glDeleteProgram(m_ProgramId);
    m_ProgramId = 0;
    m_CameraUniform = -1;
}

bool Shader::Initialize()
{
    uint vertexShaderId = CompileShader(GL_VERTEX_SHADER, GetVertexShaderCode());
    if(vertexShaderId == 0)
        return false;

    uint fragmentShaderId = CompileShader(GL_FRAGMENT_SHADER, GetFragmentShaderCode());
    if(fragmentShaderId == 0)
    {
        glDeleteShader(vertexShaderId);
        return false;
    }

    m_ProgramId = CompileProgram(vertexShaderId, fragmentShaderId);

    glDeleteShader(vertexShaderId);
    glDeleteShader(fragmentShaderId);

    return m_ProgramId != 0 && OnInitialized() && m_CameraUniform != -1;
}

void Shader::Use(const glm::mat4& camera)
{
    glUseProgram(m_ProgramId);
    glUniformMatrix4fv(m_CameraUniform, 1, GL_FALSE, &camera[0][0]);
}
