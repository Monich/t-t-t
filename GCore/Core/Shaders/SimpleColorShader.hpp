#pragma once

#include "Shader.hpp"
#include <glm/glm.hpp>

class SimpleColorShader : public Shader
{
protected:
    const char *GetVertexShaderCode() override;

    const char *GetFragmentShaderCode() override;

public:
    void Render(const RenderData &data) override;

protected:

    bool OnInitialized() override;

private:
    int m_VboHandle{-1}; // attrib:vec2
    int m_MvpMatrix{-1}; // uni:mat4
    int m_Color{-1}; // uni:vec3
};
