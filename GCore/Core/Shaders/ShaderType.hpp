#pragma once

#include "gcoredefines.hpp"

enum ShaderType : uint8
{
    SimpleColor,

    SimpleTexture,

    ShaderTypeEnd
};