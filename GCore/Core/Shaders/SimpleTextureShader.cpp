#include "SimpleTextureShader.hpp"
#include <GL/glew.h>
#include "Core/RenderData.hpp"
#include "Core/GpuMesh.hpp"
#include "Core/Texture.hpp"

void SimpleTextureShader::Render(const RenderData &data)
{
    const auto mvp = *data.mvpMatrix;
    glUniformMatrix4fv(m_MvpMatrix, 1, GL_FALSE, &mvp[0][0]);

    glBindTexture(GL_TEXTURE_2D, data.texture->GetTextureId());
    glUniform1i(m_Texture, 0);

    glEnableVertexAttribArray(m_VboHandle);
    glEnableVertexAttribArray(m_UvHandle);

    glBindBuffer(GL_ARRAY_BUFFER, data.mesh->GetVboBufferId());
    glVertexAttribPointer(m_VboHandle, 2,GL_FLOAT,GL_FALSE,0,nullptr);

    glBindBuffer(GL_ARRAY_BUFFER, data.mesh->GetUvBufferId());
    glVertexAttribPointer(m_UvHandle, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, data.mesh->GetIndexBufferId());
    glDrawElements(  GL_TRIANGLES, (int)data.mesh->GetIndexCount(), GL_UNSIGNED_INT, nullptr);

    glDisableVertexAttribArray(m_VboHandle);
    glDisableVertexAttribArray(m_UvHandle);
}

const char *SimpleTextureShader::GetVertexShaderCode()
{
    return "#version 330 core\n"
           "in vec2 aVBOHandle;"
           "uniform mat4 uCameraMatrix;"
           "uniform mat4 uMVPMatrix;"
           "in vec2 aUV;"
           "out vec2 iaUV;"
           "void main(){"
           "    gl_Position = uCameraMatrix * uMVPMatrix * vec4(aVBOHandle, 0, 1);"
           "	iaUV = aUV;"
           "}";
}

const char *SimpleTextureShader::GetFragmentShaderCode()
{
    return "#version 330 core\n"
           "in vec2 iaUV;"
           "uniform sampler2D uTextureUsed;"
           "layout(location = 0) out vec4 color;"
           "void main() {"
           "    color = texture(uTextureUsed, iaUV);"
           "}";
}

bool SimpleTextureShader::OnInitialized()
{
    m_CameraUniform = glGetUniformLocation(m_ProgramId, "uCameraMatrix");
    m_VboHandle = glGetAttribLocation(m_ProgramId, "aVBOHandle");
    m_MvpMatrix = glGetUniformLocation(m_ProgramId, "uMVPMatrix");
    m_UvHandle = glGetAttribLocation(m_ProgramId, "aUV");
    m_Texture = glGetUniformLocation(m_ProgramId, "uTextureUsed");

    return m_VboHandle != -1 && m_MvpMatrix != -1 && m_UvHandle != -1 && m_Texture != -1;
}
