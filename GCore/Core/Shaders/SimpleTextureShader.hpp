#pragma once

#include "Shader.hpp"

class SimpleTextureShader : public Shader
{
public:
    void Render(const RenderData &data) override;

protected:
    const char *GetVertexShaderCode() override;
    const char *GetFragmentShaderCode() override;

private:
    bool OnInitialized() override;

private:
    int m_VboHandle{-1}; // attrib:vec2
    int m_MvpMatrix{-1}; // uni:mat4
    int m_UvHandle{-1}; // attrib:vec2
    int m_Texture{-1}; // uni:sample2D
};
