#pragma once
#include "Shaders/ShaderType.hpp"
#include "TextureManager.hpp"
#include <queue>

class GpuMesh;
class ShaderManager;
class Renderer;
class GraphicsOrchestrator;
class Button;
class Component;

class GameObjectManager
{
public:
    explicit GameObjectManager(GraphicsOrchestrator &graphicsOrchestrator, ShaderManager &shaderManager,
                               TextureManager &textureManager);

    void RegisterRenderer(ShaderType type, Renderer *renderer);
    void DeregisterRenderer(ShaderType type, Renderer *renderer);

    const GpuMesh *GetRectMesh();

    void RegisterMouseCallback(Button *button);
    void UnregisterMouseCallback(Button *button);

    const Texture *GetTexture(const char *path);

    void OnComponentAdded(Component *component);
    void InitializeNewComponents();

private:
    GraphicsOrchestrator &m_GraphicsOrchestrator;
    ShaderManager &m_ShaderManager;
    TextureManager &m_TextureManager;
    std::queue<Component *> m_InitializationQueue;
};
