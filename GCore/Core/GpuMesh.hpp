#pragma once

#include <glm/glm.hpp>

class GpuMesh
{
public:
    GpuMesh();
    ~GpuMesh();

    void Upload(const unsigned int *indices, unsigned int indicesCount, const glm::vec2 *vertices,
                unsigned int verticesCount, const glm::vec2 *uvs, unsigned uvCount);

    [[nodiscard]] unsigned GetVboBufferId() const;
    [[nodiscard]] unsigned GetIndexBufferId() const;
    [[nodiscard]] unsigned GetUvBufferId() const;
    [[nodiscard]] long GetIndexCount() const;

private:
    long m_IndexCount;
    unsigned int m_VboBufferId;
    unsigned int m_IndexBufferId;
    unsigned int m_UvBufferId;
};
