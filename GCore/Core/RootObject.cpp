#include "RootObject.hpp"

void RootObject::Tick()
{
    Update();
}

RootObject::RootObject(GameObjectManager *manager)
{
    Initialize(nullptr, manager);
    SetName("RootObject");
}
