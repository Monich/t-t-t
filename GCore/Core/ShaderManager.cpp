#include "ShaderManager.hpp"
#include "Renderer.hpp"

namespace simple_rect_data {
const unsigned verticesCount = 4;
const glm::vec2 vertices[verticesCount] = {
        glm::vec2(-0.5f, -0.5f),
        glm::vec2(0.5f, -0.5f),
        glm::vec2(-0.5f, 0.5f),
        glm::vec2(0.5f, 0.5f)
};

const unsigned uvsCount = 4;
const glm::vec2 uvs[uvsCount] = {
        glm::vec2(0.0f, 0.0f),
        glm::vec2(1.0f, 0.0f),
        glm::vec2(0.0f, 1.0f),
        glm::vec2(1.0f, 1.0f)
};

const unsigned indicesPassCount = 6;
unsigned int indices[indicesPassCount] = {
        0, 1, 2,
        2, 1, 3
};
}

ShaderManager::ShaderManager() : m_Camera(1)
{
}

bool ShaderManager::Prepare()
{
    m_BasicRectMesh.Upload(simple_rect_data::indices,
                           simple_rect_data::indicesPassCount,
                           simple_rect_data::vertices,
                           simple_rect_data::verticesCount,
                           simple_rect_data::uvs,
                           simple_rect_data::uvsCount);
    return m_SimpleColorShader.Initialize() && m_SimpleTextureShader.Initialize();
}

void ShaderManager::RegisterRenderer(ShaderType type, Renderer *renderer)
{
    if (m_RegisteredRenderers.find(type) == m_RegisteredRenderers.end())
    {
        std::list<Renderer*> rendererList;
        rendererList.push_back(renderer);
        m_RegisteredRenderers[type] = rendererList;
    }
    else
    {
        m_RegisteredRenderers[type].push_back(renderer);
    }
}

void ShaderManager::RunAllRenderers()
{
    for (auto& shaderTypeCollection : m_RegisteredRenderers)
    {
        if(shaderTypeCollection.second.empty())
            continue;

        auto shader = GetShader(shaderTypeCollection.first);
        if(shader == nullptr)
            continue;

        shader->Use(m_Camera);

        for(auto renderer : shaderTypeCollection.second)
        {
            auto data = renderer->GetRendererData();
            shader->Render(data);
        }

        shaderTypeCollection.second.clear();
    }
}

Shader *ShaderManager::GetShader(ShaderType type)
{
    if(type == SimpleColor)
        return &m_SimpleColorShader;
    if(type == SimpleTexture)
        return &m_SimpleTextureShader;

    return nullptr;
}

void ShaderManager::DeregisterRenderer(ShaderType type, Renderer *renderer)
{
    auto it = m_RegisteredRenderers.find(type);
    if (it != m_RegisteredRenderers.end())
    {
        std::list<Renderer*>& rendererList = it->second;

        rendererList.remove(renderer);

        if (rendererList.empty())
        {
            m_RegisteredRenderers.erase(it);
        }
    }
}

const GpuMesh *ShaderManager::GetRectMesh()
{
    return &m_BasicRectMesh;
}

void ShaderManager::SetCamera(const glm::mat4 &mat)
{
    m_Camera = mat;
}

const glm::mat4 &ShaderManager::GetCamera() const
{
    return m_Camera;
}

