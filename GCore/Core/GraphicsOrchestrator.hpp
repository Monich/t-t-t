#pragma once

#include <list>

class ShaderManager;
class GLFWwindow;
class RootObject;
class Button;
class GameObjectManager;

class GraphicsOrchestrator
{
public:
    GraphicsOrchestrator(ShaderManager &m_ShaderManager, RootObject &rootObject, GameObjectManager &gameObjectManager);
    ~GraphicsOrchestrator();

    bool Prepare();
    int EnterMailLoop();

    void RegisterMouseCallback(Button *button);
    void UnregisterMouseCallback(Button *button);

private:
    ShaderManager &m_ShaderManager;
    GameObjectManager &m_GameObjectManager;
    unsigned m_VertexArrayObjectID {0};

    RootObject &m_RootObject;
    GLFWwindow *m_Window;
    std::list<Button*> m_MouseCallbacks;

    bool ShouldClose();

    void HandleMouseInput(GLFWwindow* window, int button, int action, int mods);

    static void _HandleMouseInput(GLFWwindow* window, int button, int action, int mods);

    Button *FindButtonAtPixel(double x, double y);
};
