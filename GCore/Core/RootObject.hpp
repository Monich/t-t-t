#pragma once
#include "GameObject.hpp"

class GameObjectManager;

class RootObject : public GameObject
{
public:
    explicit RootObject(GameObjectManager *manager);

    void Tick();
};
