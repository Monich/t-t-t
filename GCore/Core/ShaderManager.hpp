#pragma once

#include <map>
#include <list>
#include "Shaders/SimpleColorShader.hpp"
#include "Core/Shaders/ShaderType.hpp"
#include "GpuMesh.hpp"
#include "Core/Shaders/SimpleTextureShader.hpp"

class Renderer;

class ShaderManager
{
public:
    ShaderManager();
    bool Prepare();
    void RegisterRenderer(ShaderType type, Renderer *renderer);
    void RunAllRenderers();

    void DeregisterRenderer(ShaderType type, Renderer *renderer);

    const GpuMesh *GetRectMesh();

    void SetCamera(const glm::mat4 &mat);
    const glm::mat4 &GetCamera() const;

private:
    SimpleColorShader m_SimpleColorShader;
    SimpleTextureShader m_SimpleTextureShader;
    GpuMesh m_BasicRectMesh;
    glm::mat4 m_Camera;

    std::map<ShaderType, std::list<Renderer*>> m_RegisteredRenderers;

    Shader *GetShader(ShaderType type);

};
