#pragma once

#include <map>

class Texture;

class TextureManager
{
public:
    TextureManager();
    ~TextureManager();

    const Texture *LoadTexture(const char *path);

private:
    std::map<const char*, Texture*> m_TextureDictionary;
};
