#pragma once

class Texture
{
public:
    Texture();
    ~Texture();

    bool LoadFromFile(const char *path);

    [[nodiscard]] unsigned GetTextureId() const;

private:
    unsigned m_TextureId;
};
