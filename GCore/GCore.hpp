#pragma once

#include "Core/GraphicsOrchestrator.hpp"
#include "Core/RootObject.hpp"
#include "Core/ShaderManager.hpp"
#include "Core/GameObjectManager.hpp"
#include "Core/TextureManager.hpp"

class GameInitializer;

class GCore
{
public:
    GCore();
    bool Prepare(GameInitializer &initializer);

    int Launch();

    GameObject *GetRootObject();

    void SetCamera(const glm::mat4 &mat);

private:
    GraphicsOrchestrator m_Orchestrator;
    ShaderManager m_ShaderManager;
    GameObjectManager m_GameObjectManager;
    TextureManager m_TextureManager;
    RootObject m_RootObject;
};

