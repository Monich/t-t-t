#pragma once

#include <list>
#include <glm/glm.hpp>
#include "CanvasType.hpp"

class GameObjectManager;
class Component;

class GameObject
{
public:
    GameObject();
    ~GameObject();

    GameObject *AddChild();
    GameObject *AddChild(const char *name);

    template<class T>
    T *AddComponent();
    template<class T>
    T *AddComponent(T *component);
    void AddComponent(Component *component);

    void RemoveComponent(Component *component);

    void SetName(const char *name);
    void SetPosition(float x, float y);
    void SetScale(float width, float height);
    void SetCanvasType(CanvasType canvasType);

    [[nodiscard]] const glm::mat4 *GetMvpMatrix() const;
    glm::vec2 GetWorldPosition() const;
    glm::vec2 GetWorldScale() const;

protected:
    void Update();

    void Initialize(const GameObject *parent, GameObjectManager *manager);
private:
    const char* m_Name;
    glm::vec2 m_Position;
    float m_Rotation;
    glm::vec2 m_Scale;
    CanvasType m_CanvasType;

    mutable glm::mat4 m_MvpMatrix;

    const GameObject* m_Parent;
    std::list<GameObject*> m_Children;
    std::list<Component*> m_Components;

    GameObjectManager *m_GameObjectManager;

    void ReloadMvpMatrix() const;
    void IndicateMvpNeedsReload();

    mutable bool m_MvpNeedsReload;
};

template<class T>
T *GameObject::AddComponent()
{
    auto component = new T();
    AddComponent(component);
    return component;
}

template<class T>
T *GameObject::AddComponent(T *component)
{
    AddComponent((Component*) component);
    return component;
}
