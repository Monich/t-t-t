#include "GameObject.hpp"
#include "Component.hpp"
#include <glm/gtx/transform.hpp>
#include <algorithm>
#include "Core/GameObjectManager.hpp"

GameObject::GameObject() :
        m_Position(0, 0),
        m_Rotation(0),
        m_Scale(1, 1),
        m_GameObjectManager(nullptr),
        m_Parent(nullptr),
        m_MvpMatrix(1),
        m_MvpNeedsReload(false),
        m_Name(),
        m_CanvasType(FullRelative)
{

}

GameObject::~GameObject()
{
    for (auto component: m_Components)
        delete component;

    for (auto child: m_Children)
        delete child;
}

GameObject *GameObject::AddChild()
{
    auto newChild = new GameObject();
    m_Children.push_back(newChild);
    newChild->Initialize(this, m_GameObjectManager);
    return newChild;
}

GameObject *GameObject::AddChild(const char *name)
{
    auto child = AddChild();
    child->SetName(name);
    return child;
}

void GameObject::AddComponent(Component *component)
{
    component->Attach(this);
    m_Components.push_back(component);
    m_GameObjectManager->OnComponentAdded(component);
}

void GameObject::RemoveComponent(Component *component)
{
    auto hit = std::find(m_Components.begin(), m_Components.end(), component);
    if(hit != m_Components.end())
    {
        m_Components.erase(hit);
        delete component;
    }
}

void GameObject::SetName(const char *name)
{
    m_Name = name;
}

void GameObject::SetPosition(float x, float y)
{
    m_Position = glm::vec2(x, y);
    IndicateMvpNeedsReload();
}

void GameObject::SetScale(float width, float height)
{
    m_Scale = glm::vec2(width, height);
    IndicateMvpNeedsReload();
}

void GameObject::SetCanvasType(CanvasType canvasType)
{
    m_CanvasType = canvasType;
    IndicateMvpNeedsReload();
}

#pragma clang diagnostic push

#pragma ide diagnostic ignored "misc-no-recursion"

glm::vec2 GameObject::GetWorldPosition() const
{
    auto parentPosition = glm::vec2(0);

    if(m_Parent)
    {
        if (m_CanvasType & (RelateToParent | RelateToCenter))
            parentPosition = m_Parent->GetWorldPosition();
    }

    return m_Position + parentPosition;
}

glm::vec2 GameObject::GetWorldScale() const
{
    auto parentPosition = glm::vec2(1);

    if(m_Parent)
    {
        if (m_CanvasType & RelateToParent)
            parentPosition = m_Parent->GetWorldScale();
    }

    return m_Scale * parentPosition;
}

const glm::mat4 *GameObject::GetMvpMatrix() const
{
    if (m_MvpNeedsReload)
        ReloadMvpMatrix();
    return &m_MvpMatrix;
}

void GameObject::Update()
{
    for (const auto &component: m_Components)
    {
        component->Update();
    }

    for (const auto &child: m_Children)
    {
        child->Update();
    }
}

#pragma clang diagnostic pop

void GameObject::Initialize(const GameObject *parent, GameObjectManager *manager)
{
    m_Parent = parent;
    m_GameObjectManager = manager;

    IndicateMvpNeedsReload();
}

void GameObject::ReloadMvpMatrix() const
{
    using namespace glm;
    float d_2_r = M_PI / 180;
    vec3 rotationVertex = vec3(0, 0, 1);

    mat4 masterMatrix(1);
    if(m_Parent)
    {
        if (m_CanvasType & RelateToParent)
            masterMatrix = *m_Parent->GetMvpMatrix();
        else if (m_CanvasType & RelateToCenter)
            masterMatrix = translate(vec3(m_Parent->m_Position, 0));
    }

    m_MvpMatrix = masterMatrix *
                  translate(vec3(m_Position, 0)) *
                  rotate(m_Rotation * d_2_r, rotationVertex) *
                  scale(glm::vec3(m_Scale, 1));

    m_MvpNeedsReload = false;
}

void GameObject::IndicateMvpNeedsReload()
{
    m_MvpNeedsReload = true;
}
