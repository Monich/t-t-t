#include "Button.hpp"
#include "Core/GameObjectManager.hpp"

Button::Button(RectCollider *collider) : m_Collider(collider)
{

}

Button::~Button()
{
    m_GameObjectManager->UnregisterMouseCallback(this);
}

void Button::Initialize()
{
    m_GameObjectManager->RegisterMouseCallback(this);
}

void Button::OnPress()
{
    if(m_OnPressHandler)
        m_OnPressHandler->OnPress();
}

void Button::SetHandler(Button::OnPressHandler *handler)
{
    m_OnPressHandler = handler;
}

const RectCollider &Button::GetCollider() const
{
    return *m_Collider;
}
