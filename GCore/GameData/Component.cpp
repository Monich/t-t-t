#include "Component.hpp"
#include "GameObject.hpp"

void Component::Initialize(GameObjectManager *gameObjectManager)
{
    m_GameObjectManager = gameObjectManager;
    Initialize();
}

GameObject *Component::GetGameObject() const
{
    return m_Parent;
}

void Component::Attach(GameObject *parent)
{
    m_Parent = parent;
}
