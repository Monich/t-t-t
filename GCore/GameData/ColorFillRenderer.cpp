#include "ColorFillRenderer.hpp"
#include "Core/Shaders/ShaderType.hpp"
#include "Core/GameObjectManager.hpp"

ColorFillRenderer::ColorFillRenderer(uint8 r, uint8 g, uint8 b) : Renderer(ShaderType::SimpleColor)
{
    ChangeColor(r, g, b);
}

void ColorFillRenderer::ChangeColor(int r, int g, int b)
{
    m_RenderData.colorData = glm::vec3((float)r / 255, (float)g / 255, (float)b / 255);
}

void ColorFillRenderer::InitializeRenderer()
{
    m_RenderData.mesh = m_GameObjectManager->GetRectMesh();
}
