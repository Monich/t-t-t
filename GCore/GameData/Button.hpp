#pragma once

#include "Component.hpp"

class RectCollider;

class Button : public Component
{
public:
    class OnPressHandler;

    explicit Button(RectCollider *collider);
    ~Button() override;

    void Initialize() final;

    void OnPress();

    void SetHandler(OnPressHandler *handler);

    [[nodiscard]] const RectCollider &GetCollider() const;

private:
    RectCollider *m_Collider;
    OnPressHandler *m_OnPressHandler = nullptr;

public:
    class OnPressHandler
    {
    public:
        virtual ~OnPressHandler() = default;
        virtual void OnPress() = 0;
    };
};
