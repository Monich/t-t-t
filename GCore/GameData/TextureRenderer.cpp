#include "TextureRenderer.hpp"
#include "Core/GameObjectManager.hpp"

TextureRenderer::TextureRenderer(const char *path) : Renderer(SimpleTexture), m_Path(path)
{}

void TextureRenderer::InitializeRenderer()
{
    m_RenderData.texture = m_GameObjectManager->GetTexture(m_Path);
    m_RenderData.mesh = m_GameObjectManager->GetRectMesh();
}
