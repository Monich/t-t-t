#pragma once

#include "Component.hpp"

class RectCollider : public Component
{

public:
    [[nodiscard]] bool Collides(double x, double y) const;
};
