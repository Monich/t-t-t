#pragma once
#include <memory>

class GameObject;
class GameObjectManager;

class Component
{
public:
    virtual ~Component() = default;

    virtual void Initialize() {};
    virtual void Update() {};

    void Initialize(GameObjectManager *gameObjectManager);

    void Attach(GameObject *parent);
    [[nodiscard]] GameObject *GetGameObject() const;

protected:
    GameObject *m_Parent = nullptr;
    GameObjectManager *m_GameObjectManager = nullptr;
};
