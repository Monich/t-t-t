#pragma once
#include "gcoredefines.hpp"
#include "Core/Renderer.hpp"

class ColorFillRenderer : public Renderer
{
public:
    ColorFillRenderer(uint8 r, uint8 g, uint8 b);
    ColorFillRenderer(glm::vec3 color);

    void ChangeColor(int r, int g, int b);
protected:
    void InitializeRenderer() override;
};
