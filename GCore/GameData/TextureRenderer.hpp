#pragma once

#include "Core/Renderer.hpp"

class TextureRenderer : public Renderer
{
public:
    explicit TextureRenderer(const char* path);

protected:
    void InitializeRenderer() override;

private:
    const char* m_Path;
};
