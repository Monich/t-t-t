#include "RectCollider.hpp"
#include "GameObject.hpp"

bool RectCollider::Collides(double x, double y) const
{
    auto pos = m_Parent->GetWorldPosition();
    auto scale = m_Parent->GetWorldScale();

    auto halfScale = scale / 2.f;
    double minX = pos.x - halfScale.x;
    double maxX = pos.x + halfScale.x;
    double minY = pos.y - halfScale.y;
    double maxY = pos.y + halfScale.y;

    return x >= minX && x <= maxX && y >= minY && y <= maxY;
}
