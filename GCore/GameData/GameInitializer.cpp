#include "GameInitializer.hpp"
#include "GCore.hpp"

GameInitializer::GameInitializer(GCore &engine) : m_Engine(engine)
{}

GameObject *GameInitializer::GetRootObject()
{
    return m_Engine.GetRootObject();
}

void GameInitializer::SetCamera(const glm::mat4 &camera)
{
    m_Engine.SetCamera(camera);
}
