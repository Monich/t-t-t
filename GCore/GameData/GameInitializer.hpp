#pragma once

#include <memory>
#include <glm/glm.hpp>

class GCore;
class GameObject;

class GameInitializer
{
public:
    virtual ~GameInitializer() = default;

    virtual bool Initialize() = 0;

protected:
    explicit GameInitializer(GCore &engine);

    GameObject *GetRootObject();
    void SetCamera(const glm::mat4 &camera);

private:
    GCore &m_Engine;
};
