#pragma once

#include "gcoredefines.hpp"

enum CanvasTypeFlags : uint8
{
    RelateToCenter = 1,
    RelateToParent = 2
};

enum CanvasType : uint8
{
    NonRelative = 0,
    ToCenterUnscaled = RelateToCenter,
    FullRelative = RelateToParent,
};