message(STATUS "Looking for libraries...")
find_package(GLFW3 REQUIRED)
find_package(GLEW REQUIRED)
find_package(OpenGL REQUIRED)
find_package(DevIL REQUIRED)

include_directories(${GLFW3_INCLUDE_DIR} ${GLEW_INCLUDE_DIR} ${IL_INCLUDE_DIR})

file(GLOB_RECURSE SRCS "${CMAKE_CURRENT_LIST_DIR}/*.cpp")
file(GLOB_RECURSE INCS "${CMAKE_CURRENT_LIST_DIR}/*.hpp")
include_directories(${CMAKE_CURRENT_LIST_DIR})
include_directories(${CMAKE_CURRENT_LIST_DIR}/GameData)
add_library(GCore ${SRCS} ${INCS})

target_link_libraries(GCore ${GLFW3_LIBRARY})
target_link_libraries(GCore ${GLEW_LIBRARIES})
target_link_libraries(GCore ${OPENGL_LIBRARIES})
target_link_libraries(GCore ${IL_LIBRARIES} ${ILU_LIBRARIES} ${ILUT_LIBRARIES})
