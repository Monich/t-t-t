#pragma once

#include <cstdint>

typedef uint8_t uint8;

typedef unsigned int uint;
typedef int32_t int32;
