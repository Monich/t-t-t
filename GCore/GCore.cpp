#include "GCore.hpp"

#include "GameInitializer.hpp"

GCore::GCore() :
        m_RootObject(&m_GameObjectManager),
        m_Orchestrator(m_ShaderManager, m_RootObject, m_GameObjectManager),
        m_GameObjectManager(m_Orchestrator, m_ShaderManager, m_TextureManager)
{

}

bool GCore::Prepare(GameInitializer &initializer)
{
    return m_Orchestrator.Prepare() && initializer.Initialize();
}

int GCore::Launch()
{
    return m_Orchestrator.EnterMailLoop();
}

GameObject *GCore::GetRootObject()
{
    return &m_RootObject;
}

void GCore::SetCamera(const glm::mat4 &mat)
{
    m_ShaderManager.SetCamera(mat);
}
