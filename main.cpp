#include <cstdio>
#include "GCore.hpp"
#include "TTTInitializer.hpp"

int main()
{
    GCore gCore{};
    TTTInitializer game(gCore);
    bool gCoreReady = gCore.Prepare(game);

    if(!gCoreReady)
    {
        printf("GCore is not ready!");
        return 1;
    }

    return gCore.Launch();
}
