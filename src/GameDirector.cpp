#include <iostream>
#include "GameDirector.hpp"
#include "GameObject.hpp"
#include "ColorFillRenderer.hpp"
#include "TicTacToeField.hpp"
#include "HumanPlayer.hpp"
#include "AiPlayer.hpp"
#include "GameStatusResolver.hpp"

namespace {
const float lineOffset = 71.5f;
const int lineWidth = 30;
const int lineHeight = 400;
const int fieldSide = 113;
const int fieldOffset = 143;
}

GameDirector::GameDirector()
{
    m_Fields = new TicTacToeField*[9];

    m_CrossPlayer = new HumanPlayer(this);
    m_CirclePlayer = new AiPlayer(Circle, this);
}

GameDirector::~GameDirector()
{
    delete[] m_Fields;
    delete m_CrossPlayer;
    delete m_CirclePlayer;
}

bool GameDirector::PlayField(uint8 fieldId, Player *player)
{
    if(GetCurrentPlayer() != player)
    {
        std::cout << "This player has no move now!" << std::endl;
        return false;
    }

    if(GameStatusResolver::IsMoveValid(m_GameStatus, fieldId))
    {
        m_GameStatus = GameStatusResolver::MakeMove(m_GameStatus, fieldId, m_CurrentPlayer);
        m_CurrentPlayer = m_CurrentPlayer == Cross ? Circle : Cross;
        CheckIfGameOver();
        UpdateFields();
        GetCurrentPlayer()->NotifyTurnReady(m_GameStatus);
        return true;
    }
    return false;
}

void GameDirector::OnFieldPress(uint8 fieldId)
{
    GetCurrentPlayer()->NotifyFieldClicked(fieldId);
}

void GameDirector::Initialize()
{
    m_Parent->SetPosition(320, 240);
    m_Parent->SetScale(640*0.9f, 480*0.9f);
    m_Parent->AddComponent(new ColorFillRenderer(100, 100, 100));

    CreatePlayField();
    CreateButtons();
}

void GameDirector::CreatePlayField()
{
    auto element = m_Parent->AddChild("play_field_vertical_1");
    element->SetScale(lineWidth, lineHeight);
    element->SetPosition(-lineOffset, 0);
    element->SetCanvasType(ToCenterUnscaled);
    element->AddComponent(new ColorFillRenderer(255,255,255));

    element = m_Parent->AddChild("play_field_vertical_2");
    element->SetScale(lineWidth, lineHeight);
    element->SetPosition(lineOffset, 0);
    element->SetCanvasType(ToCenterUnscaled);
    element->AddComponent(new ColorFillRenderer(255,255,255));

    element = m_Parent->AddChild("play_field_horizontal_1");
    element->SetScale(lineHeight, lineWidth);
    element->SetPosition(0, -lineOffset);
    element->SetCanvasType(ToCenterUnscaled);
    element->AddComponent(new ColorFillRenderer(255,255,255));

    element = m_Parent->AddChild("play_field_horizontal_2");
    element->SetScale(lineHeight, lineWidth);
    element->SetPosition(0, lineOffset);
    element->SetCanvasType(ToCenterUnscaled);
    element->AddComponent(new ColorFillRenderer(255,255,255));
}

void GameDirector::CreateButtons()
{
    for (uint8 i = 0; i < 9; ++i)
    {
        m_Fields[i] = new TicTacToeField(this, i);
        auto child = m_Parent->AddChild();
        child->AddComponent(m_Fields[i]);
        child->SetScale(fieldSide, fieldSide);
    }

    m_Fields[0]->GetGameObject()->SetPosition(-fieldOffset, -fieldOffset);
    m_Fields[1]->GetGameObject()->SetPosition(0, -fieldOffset);
    m_Fields[2]->GetGameObject()->SetPosition(fieldOffset, -fieldOffset);
    m_Fields[3]->GetGameObject()->SetPosition(-fieldOffset, 0);
    m_Fields[4]->GetGameObject()->SetPosition(-0, 0);
    m_Fields[5]->GetGameObject()->SetPosition(fieldOffset, 0);
    m_Fields[6]->GetGameObject()->SetPosition(-fieldOffset, fieldOffset);
    m_Fields[7]->GetGameObject()->SetPosition(-0, fieldOffset);
    m_Fields[8]->GetGameObject()->SetPosition(fieldOffset, fieldOffset);
}

void GameDirector::UpdateFields()
{
    for (uint8 i = 0; i < 9; ++i)
    {
        auto state = static_cast<FieldStatus>((m_GameStatus >> (2 * i)) & 0x3);
        m_Fields[i]->ChangeState(state);
    }
}

void GameDirector::CheckIfGameOver()
{
    auto winner = GameStatusResolver::GetWinner(m_GameStatus);
    if(winner != None || GameStatusResolver::IsGameFinished(m_GameStatus))
        m_GameStatus = 0;
}

Player *GameDirector::GetCurrentPlayer() const
{
    if(m_CurrentPlayer == Cross)
        return m_CrossPlayer;
    else if(m_CurrentPlayer == Circle)
        return m_CirclePlayer;
    else return nullptr;
}
