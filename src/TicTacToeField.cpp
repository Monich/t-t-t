#include "TicTacToeField.hpp"
#include "GameObject.hpp"
#include "ColorFillRenderer.hpp"
#include "RectCollider.hpp"
#include "GameDirector.hpp"
#include "TextureRenderer.hpp"

TicTacToeField::TicTacToeField(GameDirector *parent, uint8 index) : m_Index(index), m_GameDirector(parent)
{
}

void TicTacToeField::Initialize()
{
    m_Parent->SetCanvasType(ToCenterUnscaled);
    auto collider = m_Parent->AddComponent<RectCollider>();
    auto button = m_Parent->AddComponent(new Button(collider));
    button->SetHandler(this);
}

void TicTacToeField::OnPress()
{
    m_GameDirector->OnFieldPress(m_Index);
}

void TicTacToeField::ChangeState(FieldStatus fieldStatus)
{
    if(fieldStatus != m_FieldStatus)
    {
        m_FieldStatus = fieldStatus;
        if(m_Renderer)
            m_Parent->RemoveComponent(m_Renderer);

        switch (m_FieldStatus)
        {
            case FieldUnchecked:
                m_Renderer = nullptr;
                break;
            case FieldCross:
//                m_Renderer = new ColorFillRenderer(255, 0, 0);
                m_Renderer = new TextureRenderer("assets/tic.png");
                break;
            case FieldCircle:
//                m_Renderer = new ColorFillRenderer(0, 255, 0);
                m_Renderer = new TextureRenderer("assets/tac.png");
                break;
        }

        if(m_Renderer)
            m_Parent->AddComponent(m_Renderer);
    }
}

FieldStatus TicTacToeField::GetState()
{
    return m_FieldStatus;
}

