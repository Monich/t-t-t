#pragma once

#include "GameData/GameInitializer.hpp"

class TTTInitializer : public GameInitializer
{
public:
    explicit TTTInitializer(GCore &core);

    bool Initialize() override;
};
