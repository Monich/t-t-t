#pragma once

#include "Component.hpp"
#include "Button.hpp"
#include "FieldStatus.hpp"

class Renderer;
class GameDirector;

class TicTacToeField : public Component, public Button::OnPressHandler
{
public:
    TicTacToeField(GameDirector *parent, uint8 index);
    void Initialize() override;

    void OnPress() override;

    void ChangeState(FieldStatus fieldStatus);

    FieldStatus GetState();

private:
    const uint8 m_Index;
    FieldStatus m_FieldStatus = FieldUnchecked;
    Renderer *m_Renderer = nullptr;
    GameDirector *m_GameDirector;
};
