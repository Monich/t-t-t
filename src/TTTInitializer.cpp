#include <glm/ext/matrix_clip_space.hpp>
#include "TTTInitializer.hpp"
#include "GameData/GameObject.hpp"
#include "GameDirector.hpp"

TTTInitializer::TTTInitializer(GCore &core) : GameInitializer(core)
{}

bool TTTInitializer::Initialize()
{
    glm::mat4 camera = glm::ortho(0.f, 640.f, 480.f, 0.f);
    SetCamera(camera);

    auto rootObj = GetRootObject();
    if (auto background = rootObj->AddChild("GameDirector"))
    {
        background->AddComponent<GameDirector>();
        return true;
    }

    return false;
}
