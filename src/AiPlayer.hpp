#pragma once

#include <thread>
#include "Player.hpp"
#include "PlayerSign.hpp"

class AiPlayer : public Player
{
public:
    AiPlayer(PlayerSign sign, GameDirector *parent);
    ~AiPlayer() override;

    void NotifyTurnReady(int32 gameStatus) override;

    void NotifyFieldClicked(uint8 fieldId) override;

private:
    std::thread m_CalculationThread;
    const PlayerSign m_Sign;
    volatile bool m_ShouldQuit;

    void CalculationMethod(int gameStatus);
};
