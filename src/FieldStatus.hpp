#pragma once

#include "gcoredefines.hpp"

enum FieldStatus : uint8
{
    FieldUnchecked,
    FieldCross,
    FieldCircle
};
