#include "GameStatusResolver.hpp"

namespace {
const int winPatterns[8] = {
        0b010101000000000000,
        0b000000010101000000,
        0b000000000000010101,
        0b010000010000010000,
        0b000100000100000100,
        0b000001000001000001,
        0b010000000100000001,
        0b000001000100010000
};
}

bool GameStatusResolver::IsGameFinished(int32 gameStatus)
{
    int mask = 0b010101010101010101;
    return ((gameStatus | (gameStatus >> 1)) & mask) == mask;
}

PlayerSign GameStatusResolver::GetWinner(int32 gameStatus)
{
    int mask = 0b010101010101010101;
    int crossPattern = gameStatus & mask;
    int circlePattern = (gameStatus >> 1) & mask;

    for (int winPattern : winPatterns) {
        if((crossPattern & winPattern) == winPattern)
            return Cross;
        if((circlePattern & winPattern) == winPattern)
            return Circle;
    }

    return None;
}

bool GameStatusResolver::IsMoveValid(int32 gameState, int move)
{
    return (gameState >> (move*2) & 0x3) == 0;
}

int32 GameStatusResolver::MakeMove(int32 state, int move, int sign)
{
    int shiftAmount  = move * 2;
    state &= ~(0x3 << shiftAmount);
    state |= (sign << shiftAmount);
    return state;
}
