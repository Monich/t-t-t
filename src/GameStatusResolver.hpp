#pragma once

#include "PlayerSign.hpp"

class GameStatusResolver
{
public:
    static bool IsGameFinished(int32 gameStatus);
    static PlayerSign GetWinner(int32 gameStatus);
    static bool IsMoveValid(int32 gameState, int move);
    static int32 MakeMove(int32 state, int move, int sign);
};
