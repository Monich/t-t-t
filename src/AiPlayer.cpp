#include <random>
#include <climits>
#include "AiPlayer.hpp"
#include "GameDirector.hpp"
#include "GameStatusResolver.hpp"

namespace {

int minMax(int32 gameState, int &value, int sign)
{
    if(auto winner = GameStatusResolver::GetWinner(gameState))
    {
        if(winner == sign)
            value = 10;
        else value = -10;
        return -1;
    }
    if(GameStatusResolver::IsGameFinished(gameState))
    {
        value = 0;
        return -1;
    }

    int bestMove = -1;
    value = INT_MIN;
    for (int move = 0; move < 9; ++move)
    {
        if(GameStatusResolver::IsMoveValid(gameState, move))
        {
            int newValue;
            auto newGameState = GameStatusResolver::MakeMove(gameState, move, sign);
            minMax(newGameState, newValue, sign == Cross ? Circle : Cross);
            newValue = -newValue;
            if(value < newValue)
            {
                bestMove = move;
                value = newValue;
            }
        }
    }

    return bestMove;
}

}

AiPlayer::AiPlayer(PlayerSign sign, GameDirector *parent) : Player(parent), m_Sign(sign)
{}

AiPlayer::~AiPlayer()
{
    m_ShouldQuit = true;
    if(m_CalculationThread.joinable())
        m_CalculationThread.join();
}

void AiPlayer::NotifyTurnReady(int32 gameStatus)
{
    if(m_CalculationThread.joinable())
        m_CalculationThread.join();

    std::thread newThread(&AiPlayer::CalculationMethod, this, gameStatus);
    m_CalculationThread.swap(newThread);
}

void AiPlayer::NotifyFieldClicked(uint8 fieldId)
{}

void AiPlayer::CalculationMethod(int gameStatus)
{
    std::this_thread::sleep_for(std::chrono::seconds(1));
    if(m_ShouldQuit)
        return;

    int stub;
    auto move = minMax(gameStatus, stub, m_Sign);

    if(m_ShouldQuit)
        return;

    m_GameDirector->PlayField(move, this);
}

