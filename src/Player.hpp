#pragma once

#include "gcoredefines.hpp"

class GameDirector;

class Player
{
public:
    virtual ~Player() = default;
    virtual void NotifyTurnReady(int32 gameStatus) = 0;
    virtual void NotifyFieldClicked(uint8 fieldId) = 0;

protected:
    explicit Player(GameDirector *gameDirector) : m_GameDirector(gameDirector)
    {}

    GameDirector* m_GameDirector;
};
