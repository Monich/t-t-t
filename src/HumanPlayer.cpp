#include "HumanPlayer.hpp"
#include "GameDirector.hpp"

HumanPlayer::HumanPlayer(GameDirector *gameDirector) : Player(gameDirector)
{
}

void HumanPlayer::NotifyTurnReady(int32 gameStatus)
{}

void HumanPlayer::NotifyFieldClicked(uint8 fieldId)
{
    m_GameDirector->PlayField(fieldId, this);
}
