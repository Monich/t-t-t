#pragma once

#include "gcoredefines.hpp"

enum PlayerSign : uint8
{
    None,
    Cross,
    Circle
};
