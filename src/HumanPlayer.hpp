#pragma once

#include "Player.hpp"
#include "gcoredefines.hpp"

class HumanPlayer : public Player
{
public:
    explicit HumanPlayer(GameDirector *gameDirector);
    ~HumanPlayer() override = default;

    void NotifyTurnReady(int32 gameStatus) override;

    void NotifyFieldClicked(uint8 fieldId) override;
};
