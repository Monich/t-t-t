#pragma once

#include "Component.hpp"
#include "PlayerSign.hpp"

class TicTacToeField;
class Player;

class GameDirector : public Component
{
public:
    GameDirector();
    ~GameDirector() override;

    bool PlayField(uint8 fieldId, Player *player);
    void OnFieldPress(uint8 fieldId);
private:
    TicTacToeField** m_Fields;
    int32 m_GameStatus = 0;
    PlayerSign m_CurrentPlayer = Cross;

    Player *m_CrossPlayer;
    Player *m_CirclePlayer;

    void Initialize() override;
    void CreatePlayField();
    void CreateButtons();

    void UpdateFields();

    void CheckIfGameOver();
    [[nodiscard]] Player *GetCurrentPlayer() const;
};
